//Sorts a table by clicking a th. Looks for the data attribute 'data-sort-val',
//and sorts by that if it exists. If not the text of the td is used. 
//Toggles sortdirection by a data attribute on the th.
$('.sortable').on("click", "th", function () {

    var th = $(this);
    var index = th.index();
    var dir = th.data('sortDirection');
    var rows = th.parents("table").find("tbody tr");

    rows.sort(function (a, b) {
        var aVal = '' + ($('td', a).eq(index).data("sortVal") || $('td', a).eq(index).text());
        var bVal = '' + ($('td', b).eq(index).data("sortVal") || $('td', b).eq(index).text());
        if (dir == 'asc') {
            th.data('sortDirection', 'desc');
            return aVal.localeCompare(bVal);
        } else {
            th.data('sortDirection', 'asc');
            return bVal.localeCompare(aVal);
        }
    }).appendTo(rows.parent());
});